//Based on:
//https://www.atnf.csiro.au/people/mcalabre/WCS/
//	twcshdr.c
//-----------------------------------------------------------------------------
#include <ctype.h>
#include <stdio.h>
#include <string.h>
//-----------------------------------------------------------------------------
#include <fitsio.h>
#include "wcshdr.h"
#include "wcslib.h"
#include "wcsfix.h"
#include "getwcstab.h"
//-----------------------------------------------------------------------------
#define NAXIS 2
//-----------------------------------------------------------------------------
void user_info(void) {
  fprintf(stdout, "Convert from pixel to sky and back using the WCS info of a fits file\n");
  fprintf(stdout, "Usage: wcslib_conversion [-p | -w] itemSeqCount itemSeq <file>\n");
  fprintf(stdout, "\n\t Example 1. Convert from two pixel sequence (12.23,23.51) (11.8,2.3) to sky positions in decimal degree\n");
  fprintf(stdout, "\t wcslib -p 2 12.23:23.51:11.8:2.3 f.fits\n");
  fprintf(stdout, "\n\t Example 2. Convert one sky positions in decimal degree (87.0,3.11) to pixel position\n");
  fprintf(stdout, "\t wcslib -w 1 87.0:3.11 f.fits\n");
}

//-----------------------------------------------------------------------------
int run(char * filename, int item_count, double  * item_seq, double * item_converted, int dopixel, int doworld) {

  char *alt = 0x0, *header;
  int  alts[27], i, ialt, nkeyrec, nreject, nwcs, status = 0;
  fitsfile *fptr;
  struct wcsprm *wcs, *wcsi;
  int error = 0;

  // Open the FITS test file and read the primary header.
  fits_open_file(&fptr, filename, READONLY, &status);
  if ((status = fits_hdr2str(fptr, 1, NULL, 0, &header, &nkeyrec, &status))) {
    fits_report_error(stderr, status);
    error = -1;
    return error;
  }

  //-------------------------------------------------------------------------
  // Basic steps required to interpret a FITS WCS header, including -TAB.
  //-------------------------------------------------------------------------

  // Parse the primary header of the FITS file.
  if ((status = wcspih(header, nkeyrec, WCSHDR_all, 0, &nreject, &nwcs,&wcs)))
	fprintf(stderr, "wcspih ERROR %d: %s.\n", status,wcshdr_errmsg[status]);


  // Read coordinate arrays from the binary table extension.
   if ((status = fits_read_wcstab(fptr, wcs->nwtb, (wtbarr *)wcs->wtb,&status))) {
	  fits_report_error(stderr, status);
	  error = -2;
	  return error;
   }


   // Sort out alternates.
   i = 0;
   if (alt) {
     if ('0' <= *alt && *alt <= '9') {
	   if ((i = atoi(alt)) > nwcs-1) {
	     wcsfprintf(stderr, "WARNING, no alternate coordinate ""representation \"%s\".\n", alt);
	     error = -3;
	     return error;
	   }
     } else {
	   wcsidx(nwcs, &wcs, alts);

	   ialt = toupper(*alt);
	   if (strlen(alt) > 1) {
	     wcsfprintf(stderr, "WARNING, alternate specifier \"%s\" is ""invalid.\n", alt);
	     error = -4;
	     return error;
	   }
	   else if (*alt == ' ') {
	     if (alts[0] == -1) {
	       wcsfprintf(stderr, "WARNING, no primary coordinate ""representation.\n");
	       error = -5;
	       return error;
	     }
	   } else if (ialt < 'A' || ialt > 'Z') {
	       wcsfprintf(stderr, "WARNING, alternate specifier \"%s\" is invalid.\n", alt);
	       error = -6;
	       return error;
	    } else {
	        if ((i = alts[ialt - 'A' + 1]) == -1) {
	          wcsfprintf(stderr, "WARNING, no alternate coordinate representation \"%s\".\n", alt);
	          error = -7;
	          return error;
	      }
	    }
	  }
   }

   wcsi = wcs + i;

   //-------------------------------------------------------------------------
   // The wcsprm struct is now ready for use.
   //-------------------------------------------------------------------------

   // Finished with the FITS file.
  fits_close_file(fptr, &status);
  fits_free_memory(header, &status);

  // Initialize the wcsprm struct, also taking control of memory allocated byvoid
  if ((status = wcsset(wcsi))) {
    fprintf(stderr, "wcsset ERROR %d: %s.\n", status, wcs_errmsg[status]);
    error = -8;
    return error;
  }

  double  * imgcrd = malloc(sizeof(double) * item_count * 2);
  int * stat = malloc(sizeof(int) * item_count);
  double * phi = malloc(sizeof(double) * item_count);
  double * theta = malloc(sizeof(double) * item_count);
  if (dopixel) {
    status = wcsp2s(wcsi, item_count, NAXIS, item_seq, imgcrd, phi, theta, item_converted, stat);
 	if (status) error = -9;
  }
  else {
    if (doworld) {
	  status = wcss2p(wcsi, item_count, NAXIS, item_seq, phi, theta, imgcrd, item_converted, stat);
	  if (status) error = -10;
    }
	free(theta);
	free(phi);
	free(stat);
	free(imgcrd);
	wcsvfree(&nwcs, &wcs);
	return error;
  }
  return 0;
}

//-----------------------------------------------------------------------------
int xyToSky(char * filename, int item_count, double  * item_seq, double * item_converted) {
  return run(filename, item_count, item_seq, item_converted, 1, 0);
}

//-----------------------------------------------------------------------------
int skyToXY(char * filename, int item_count, double  * item_seq, double * item_converted) {
  return run(filename, item_count, item_seq, item_converted, 0, 1);
}
//-----------------------------------------------------------------------------
double * user_input(int item_count,char * user_input) {
  double  * item_seq = malloc(sizeof(double) * item_count * 2);  //freed before exit
  char * token;
  int i = 0;

  while ((token = strsep(&user_input, ":"))) {
    item_seq[i] = strtod(token,NULL);
	++i;
  }
  return item_seq;
}
//-----------------------------------------------------------------------------
int main(int argc, char *argv[]) {

  int i=0;
  char * filename = argv[argc-1];
  int item_count = atoi(argv[2]);
  double  * item_seq = user_input(item_count,argv[3]);
  double  * item_converted = malloc(sizeof(double) * item_count * 2);

  // Parse options.
  for (i = 1; i < argc && argv[i][0] == '-'; i++) {
    if (!argv[i][1]) break;

    switch (argv[i][1]) {

    case 'p':
      xyToSky(filename, item_count, item_seq, item_converted);
      break;
    case 'w':
      skyToXY(filename, item_count, item_seq, item_converted);
      break;

    default:
      user_info();
      return 1;
    }
  }

  if (argc != 5) {
	  user_info();
	  return 1;
  }

  for(i = 0; i<item_count*2;i+=2)
    printf("%20.15f\t%20.15f\n", item_converted[i], item_converted[i+1]);

  free(item_converted);
  free(item_seq);

  return 0;
}
//-----------------------------------------------------------------------------
//end of file "main.c"
//-----------------------------------------------------------------------------
